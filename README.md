# adescoms.com

Static web page powered by [hugo](https://gohugo.io)

To start the web locally: 
```
 hugo server -D
```

## Pipeline status

[![pipeline status](https://gitlab.com/adescoms/my-static-web/badges/master/pipeline.svg)](https://gitlab.com/adescoms/my-static-web/commits/master)